var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserSync = require('browser-sync');
var browserify = require('browserify');
var watchify = require('watchify');
var babel = require('babelify');
var autoprefixer = require('gulp-autoprefixer');
var sass  = require('gulp-sass');

const dirs = {
  src: 'src',
  dest: 'build'
};

const sassPaths = {
  src: `${dirs.src}/css/app.scss`,
  dest: `${dirs.dest}/styles/`
};

gulp.task('styles', () => {
  return gulp.src(sassPaths.src)
    .pipe(autoprefixer())
		.pipe(sass()) 
    .pipe(gulp.dest(sassPaths.dest))
});

var props = {
  entries: ['./src/scripts/main.js'],
  extensions: ['.js'],
  debug : true,
  cache: {},
  packageCache: {},
  transform:  [babel]
};

var bundler = watchify(browserify(props));

gulp.task('compile', () => {
  bundler.bundle()
    .on('error', function(err) { console.error(err); this.emit('end'); })
    .pipe(source('build.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./build/scripts'))
    .on('end', function() {
      	browserSync.init({
          server: {
            baseDir: "./"
          }
        }); 
    })
})

gulp.task('watch', () => {
  bundler.on('update', function() {
    compile();
  });
})

gulp.task('default', ['styles', 'compile', 'watch']);