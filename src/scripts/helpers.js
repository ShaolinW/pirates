let Helpers  = {

  baseUrl : 'http://fake-hotel-api.herokuapp.com/api/',
  // base url for API calls 

  /**
	 * Document Ready 
	 * @return 	{ Function } callback  
	 */
  documentReady: function(fn) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  },

  /**
	 * Ajax Get 
	 * @param  	{ String } url
	 * @param 	{ Object } params 
   * @return 	{ Function } succesCallback
	 * @return 	{ Function } errorCallback  
	 */
  getAjax: function(uri, succesCallback, errorCallback) {
    let request = new XMLHttpRequest();
    let url = this.baseUrl + uri;

    request.open('GET', url, true);

    request.onload = function() {
      let data = JSON.parse(request.responseText);
      if (request.status >= 200 && request.status < 400) {
        succesCallback(data);
      } else {
        errorCallback(data)
      }
    };

    request.onerror = function() {
      errorCallback(null)
    };

    request.send();
  },

  /**
	 * Repeat string n of times 
	 * @return 	{ String } 
	 */
  repeatMe: function(times, string) {
    let tmp = '';

    for (let i = 0; i < times; i++) {
      tmp += string;
    }

    return tmp;
  },

  /**
	 * Format Date
	 * @return 	{ String } 
	 */
  formatDate: function (date) {
    date = Date.parse(date);
    date = new Date(date);

    let shortDate = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
    
    return shortDate
  },

  addClass: function(el, className) {
    if (el.classList) {
      el.classList.add(className);
    } else {
      el.className += ' ' + className;
    }
  },

  removeClass: function(el, className) {
    if (el.classList) {
      el.classList.remove(className);
    } else {
      el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
  }
}

export { Helpers };