import { Helpers } from './helpers';

/**
 * Global namespace
 */
var APP = APP || {};

APP.client = (function () {

	let self = {
		defaults: {},
		instance: {}
	}

	class Hotels {
		
		/**
		 * Constructor function
		 * @param { Helpers } 	$element 	Module element
		 * @param { Object } 	options  	Module options
		 */
		constructor ($element, options) {

			this.$element = $element;
			this.options = Object.assign(true, {}, self.defaults, options);

			this.DOM = {
				$hotelList: $element.querySelectorAll('.hotel-list'),
				$loadBtn: $element.querySelectorAll('.load-button'),
				$hotelTemplate: $element.querySelectorAll('.hotel-card-template'),
				$reviewsTemplate: $element.querySelectorAll('.reviews-template'),
				$errorDiv: $element.querySelector('.error')
			}

			this.hotels = {};

			// Hotel template
			let source = this.DOM.$hotelTemplate[0].innerHTML;
			this.hotelTemplate = Handlebars.compile(source);

			// Reviews template
			let reviewSource = this.DOM.$reviewsTemplate[0].innerHTML;
			this.reviewsTemplate = Handlebars.compile(reviewSource);

			this._attachEvents();
		}
 
		/**
		 * Load hotels
		 * @param { Function } callback
		 * @return { Object } 	Object with hotels
		 */
		getHotels (fn) {
			Helpers.getAjax('hotels', (data) => {
				return fn(data, false)
			}, (error) => {
				return fn(error, true)
			});
		}

		/**
		 * Render reviews
		 * @param { int } id of the hotel
		 * @param { fn } callback
		 * @return { Object } 	Object with review
		 */
		getReviews (_id, fn) {
			let uri = 'reviews?hotel_id=' + _id;

			Helpers.getAjax(uri, (data) => {
				return fn(data)
			}, (error) => {
				return fn(data)
			});
		}

		/**
		 * Render rating
		 * @return { void }
		 */
		renderRating() {
			let ratings = this.$element.querySelectorAll('.rating');
			let starEntity = '&#9733;';

			for (let i = 0; i < ratings.length; i++) {
				let starNumber = ratings[i].dataset ? ratings[i].dataset.count : ratings[i].getAttribute('data-count');

				for (let x = 0; x < starNumber; x++) {

					ratings[i].innerHTML = Helpers.repeatMe(starNumber, starEntity);
				}
			}
		}

		/**
		 * Render dates
		 * @return { void }
		 */
		renderDates() {
			let dates = this.$element.querySelectorAll('.date');

			for (let i = 0; i < dates.length; i++) {
				let startDate = dates[i].dataset ? dates[i].dataset.start : dates[i].getAttribute('data-start');
				let endDate = dates[i].dataset ? dates[i].dataset.end : dates[i].getAttribute('data-end');

				for (let x = 0; x < dates.length; x++) {
					dates[i].innerHTML = Helpers.formatDate(startDate) + ' - ' + Helpers.formatDate(endDate); 
				}
			}	
		}

		/**
		 * Render hotels into DOM
		 * @param  { Array } 	hotels 	Hotels array
		 * @return { void }
		 */
		renderHotels (hotels) {
			let hotelHtml = this.hotelTemplate(hotels);
			this.DOM.$hotelList[0].innerHTML = hotelHtml;

			this.renderRating();
			this.renderDates();
		}

		/**
		 * Render reviews into DOM
		 * @param  { Array } 	hotels 	Reviews array
		 * @return { void }
		 */
		renderReviews (_id, reviews) {
			let reviewHtml = this.reviewsTemplate(reviews);
			let reviewsContainer = document.querySelectorAll('.review-container');

			for (let i = 0; i < reviewsContainer.length; i++) {
				let id = reviewsContainer[i].dataset ? reviewsContainer[i].dataset.id : reviewsContainer[i].getAttribute('data-id');

				if (reviewsContainer[i].dataset.id == _id) {
					reviewsContainer[i].innerHTML = reviewHtml;
				}
			}
		}

		/**
		 * Show server error 
		 */
		showError () {
			Helpers.addClass(this.DOM.$errorDiv, 'active');

			setTimeout( () => {
				Helpers.removeClass(this.DOM.$errorDiv, 'active');
			}, 3000);
		}

		/**
		 * Events
		 */
		_attachEvents () {
			// Load Hotels
			this.DOM.$loadBtn[0].addEventListener('click', () => {
					this.getHotels((data, error) => {
						this.hotels = data;
						
						if (error) {
							this.showError();
						} else {
							this.renderHotels(this.hotels.slice(0, 5));
						}
					});

			});

			document.addEventListener('click', (e) => {
				let target = e.target || e.srcElement;
			
				if (target.className == 'show-review') {
					let _id = target.dataset.id ? target.dataset.id : target.getAttribute('data-id');

					this.getReviews(_id, (data) => {
						this.renderReviews(_id, data);
					})
				}
			})
		}
	}

	/**
	 * Init module
	 * @param  	{ String } selector Helper selector
	 * @param 	{ Object } options 	Module options
	 * @return 	{ Array } Array of istances	
	 */
	self.init = function (selector, options) {
		// Add modul methods
		let elements = document.querySelectorAll(selector),
		results = [];

		Array.prototype.forEach.call(elements, function(el, index){
			let	hotels = new Hotels (el, options);
			self.instance = hotels;
			results.push(hotels);
		});
	
		return results
	}

	return self;
}());

APP.client.init('.container');


